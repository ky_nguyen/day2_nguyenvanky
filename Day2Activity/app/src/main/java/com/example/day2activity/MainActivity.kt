package com.example.day2activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val message = intent.getStringExtra("Activity_one")
        txt_title?.text = message
        setActions()
    }

    private fun setActions() {
        btn_back?.setOnClickListener {
            val resultData = Intent()
            resultData.putExtra("REQUES_CODE","tro ve ActivityOne")
            setResult(RESULT_OK,resultData)
            finish()
        }
    }
    override fun onBackPressed() {

        val resultData = Intent()
        resultData.putExtra("REQUES_CODE","tro ve ActivityOne")
        setResult(RESULT_OK,resultData)
        super.onBackPressed()
    }

}