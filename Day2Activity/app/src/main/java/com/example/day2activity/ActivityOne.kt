package com.example.day2activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_one.*

class ActivityOne : AppCompatActivity(), View.OnClickListener {
    private val REQUES_CODE = 1000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)
        addControls();
    }

    private fun addControls() {
        btnhangdau.setOnClickListener(this)
        btnnhiemvu.setOnClickListener(this)
        btnphienban.setOnClickListener(this)
        btnbatdau?.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("Activity_one","mot thong bao tu ActivityOne")
            startActivityForResult(intent,REQUES_CODE)
        }
    }
    override fun onStart() {
        super.onStart()
        Log.d("onStart","quay")
    }
    override fun onResume() {
        super.onResume()
        Log.d("onResume","turned")
    }

    override fun onPause() {
        super.onPause()
        Log.d("onPause","turned")
    }
    override fun onStop() {
        super.onStop()
        Log.d("onStop","turned")
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.d("onDetroy","turned")
    }
    override fun onRestart() {
        super.onRestart()
        Log.d("onRestart", "turned")
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            when(requestCode){
                REQUES_CODE ->{
                    val m = data?.getStringExtra("REQUES_CODE")
                    Toast.makeText(this, m,Toast.LENGTH_LONG).show()
                }
            }
        }
    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnhangdau ->
                startActivity(Intent(this, HanDauActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP))

            R.id.btnnhiemvu ->
                startActivity(Intent(this, NhiemVuActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))

            R.id.btnphienban ->
                // do khong tim thay flag singleInstance nen em set o manifest
                startActivity(Intent(this, PhienBanActivity::class.java))
        }
    }


}